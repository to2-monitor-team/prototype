package frazeusz.monitor.prototype;

import javax.swing.*;
import java.awt.*;


public class Main {
    public static void main(String[] args) {
        // all work here is done by Plotter
        final Monitor monitor = new Monitor();
        final JPanel chart = monitor.getPanel();
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                final JFrame frame = new JFrame("Frazeusz - Monitor");
                frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                frame.add(chart);
                frame.pack();
                frame.setVisible(true);
            }
        });
    }
}