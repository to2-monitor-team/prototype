package frazeusz.monitor.prototype;

import javax.swing.*;


public class Monitor {
    private final MonitorPanel panel;

    public Monitor() {
        panel = new MonitorPanel();
        (new MonitorThread(panel)).start();
    }

    public JPanel getPanel() {
        return panel;
    }

}
