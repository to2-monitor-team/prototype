package frazeusz.monitor.prototype;

import org.jfree.data.time.Millisecond;
import org.jfree.data.time.RegularTimePeriod;

import java.awt.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MonitorThread extends Thread {
    private final MonitorPanel panel;
    private final List<CrawlerMetrics> metrics;
    private double currentValue = 0.0;

    public MonitorThread(MonitorPanel panel) {
        this.panel = panel;
        metrics = new ArrayList<>();
    }

    public void run() {
        while (true) {
            try {
                // naive solution, should be based on clock time
                Thread.sleep(1000);
                updateChart();
            } catch (InterruptedException e) {
                System.err.println("Error in MonitorThread");
            }
        }
    }

    private void updateChart() {
        RegularTimePeriod currentTime = new Millisecond(new Date());
        currentValue += Math.random() + 0.5;
        CrawlerMetrics newMetrics = new CrawlerMetrics(currentValue, currentTime);
        metrics.add(newMetrics);
        if (metrics.size() > 10) {
            // On each update we add metric which timestamp
            // is exactly one second larger then previous one
            // (guaranteed by RegularTimePeriod I suppose).
            // We won't be able to use that in real Monitor,
            // because we need to show clock time on the chart.
            metrics.remove(0);
        }

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                panel.update(metrics);
            }
        });
    }
}
