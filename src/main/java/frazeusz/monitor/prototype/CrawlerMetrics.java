package frazeusz.monitor.prototype;

import org.jfree.data.time.RegularTimePeriod;


public class CrawlerMetrics {
    private final double value;
    private final RegularTimePeriod timestamp;

    public CrawlerMetrics(double value, RegularTimePeriod timestamp) {
        this.value = value;
        this.timestamp = timestamp;
    }

    public double getValue() {
        return value;
    }

    public RegularTimePeriod getTimestamp() {
        return timestamp;
    }
}
