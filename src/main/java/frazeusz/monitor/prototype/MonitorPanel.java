package frazeusz.monitor.prototype;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.general.SeriesException;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import javax.swing.*;
import java.awt.*;
import java.util.List;


public class MonitorPanel extends JPanel {
    private JFreeChart chart;
    private final TimeSeriesCollection dataSet;

    public MonitorPanel() {
        dataSet = initDataset();
        chart = initChart(dataSet);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setDomainZoomable(false);
        chartPanel.setRangeZoomable(false);
        add(chartPanel);
    }

    /**
     * On each update rewrite whole time series.
     * Adding to series is easy, but I don't know
     * how to remove first item from the series.
     * Unfortunately this causes flickering when updating chart
     * since grid in the background changes on each update :/
     */
    public void update(final List<CrawlerMetrics> metrics) {
        TimeSeries series = new TimeSeries("processed pages");
        for (CrawlerMetrics m : metrics) {
            try {
                series.add(m.getTimestamp(), m.getValue());
            } catch (SeriesException e) {
                System.err.println("Error adding to series");
            }
        }
        dataSet.removeAllSeries();
        dataSet.addSeries(series);
    }

    private TimeSeriesCollection initDataset() {
        TimeSeries series = new TimeSeries("processed pages");
        return new TimeSeriesCollection(series);
    }

    private JFreeChart initChart(XYDataset dataset) {
        chart = ChartFactory.createTimeSeriesChart(
                "Crawler status",
                "time",
                "processed pages",
                dataset,
                false,
                false,
                false);
        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setDomainGridlinePaint(Color.GRAY);
        plot.setRangeGridlinePaint(Color.GRAY);
        plot.getDomainAxis().setFixedAutoRange(60 * 60 * 1000);
        return chart;
    }
}
